'use strict'
//  1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
//  Використайте 2 способи для пошуку елементів.


let featureElements = document.body.querySelectorAll('.feature');
console.log(featureElements);

// or

// const featureElementsAnother = document.body.getElementsByClassName('feature');
// console.log(featureElementsAnother);


//  Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).


featureElements.forEach((element) => element.style.textAlign = 'center');

// or

// featureElements.forEach((element)=> element.style.cssText = 'text-align: center');


//
//  2. Змініть текст усіх елементів h2 на "Awesome feature".


let h2 = document.body.querySelectorAll('h2');
h2.forEach((element) => element.textContent = 'Awesome feature')


//
//  3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".


let featureTitle = [...document.body.getElementsByClassName('feature-title')];
featureTitle.forEach((element) => element.textContent += '!')